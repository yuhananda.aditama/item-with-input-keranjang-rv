package id.bootcamp.recyclerviewwithinput

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import id.bootcamp.recyclerviewwithinput.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var mAdapter: FoodAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //data dummy
        val dataList = ArrayList<FoodData>()
        for (i in 0..10) {
            val foodData =
                FoodData(foodName = "Food $i", foodPrice = (i * 10000L), foodQuantity = 0)
            dataList.add(foodData)
        }

        mAdapter = FoodAdapter(dataList)
        mAdapter.onFoodUpdateListener = object :FoodAdapter.OnFoodUpdateListener{
            override fun onItemChanged() {
                updateKeranjang()
            }
        }

        binding.rvFood.adapter = mAdapter
        binding.rvFood.layoutManager = GridLayoutManager(this, 2)

        updateKeranjang()
    }

    private fun updateKeranjang() {
        val dataList = mAdapter.foodList
        val filteredList = dataList.filter {
            it.foodQuantity > 0
        }
        if (filteredList.isNotEmpty()){
            binding.rlKeranjang.visibility = View.VISIBLE
            var totalBiaya = 0L
            for (i in 0..filteredList.lastIndex){
                totalBiaya += filteredList[i].foodQuantity * filteredList[i].foodPrice
            }
            binding.txtTotalPrice.text = totalBiaya.toString()

        } else{
            binding.rlKeranjang.visibility = View.GONE
        }
    }
}