package id.bootcamp.recyclerviewwithinput

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class FoodAdapter(val foodList: ArrayList<FoodData> = ArrayList()) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    var onFoodUpdateListener : OnFoodUpdateListener? = null

    interface OnFoodUpdateListener{
        fun onItemChanged()
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val foodName = itemView.findViewById<TextView>(R.id.tvFoodName)
        val foodPrice = itemView.findViewById<TextView>(R.id.tvFoodPrice)
        val btnPlus = itemView.findViewById<Button>(R.id.btnPlus)
        val btnMin = itemView.findViewById<Button>(R.id.btnMinus)
        val textQuantity = itemView.findViewById<TextView>(R.id.tvQuantity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_food,parent,false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return foodList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder){
            val data = foodList[position]
            holder.foodName.text = data.foodName
            holder.foodPrice.text = data.foodPrice.toString()
            holder.textQuantity.text = data.foodQuantity.toString()

            if (holder.textQuantity.text == "0"){
                holder.textQuantity.visibility = View.INVISIBLE
                holder.btnMin.visibility = View.INVISIBLE
            } else {
                holder.textQuantity.visibility = View.VISIBLE
                holder.btnMin.visibility = View.VISIBLE
            }

            holder.btnPlus.setOnClickListener {
                data.foodQuantity++
                foodList.set(position,data)
                notifyItemChanged(position)
                onFoodUpdateListener?.onItemChanged()
            }

            holder.btnMin.setOnClickListener {
                data.foodQuantity--
                foodList.set(position,data)
                notifyItemChanged(position)
                onFoodUpdateListener?.onItemChanged()
            }
        }
    }

}