package id.bootcamp.recyclerviewwithinput

data class FoodData (
    var foodId : Int = 0,
    var foodName : String = "",
    var foodPrice: Long = 0,
    var foodQuantity:Int = 0
)